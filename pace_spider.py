"""
Pace urlje dobimo iz picklov, ki smo jih predhodno naredili v datoteki
get_game_urls.py
Pajka zaženemo za vsako sezono posebej (pace_urls_1213.pickle,
pace_urls_1314.pickle, pace_urls_1415.pickle, pace_urls_1516.pickle)
Zagon pajka v bash: scrapy runspider pace_spider.py -a year=1213
"""
import scrapy
import pickle
import re


class GamesSpider(scrapy.Spider):
    name = "games"

    def __init__(self, year='', *args, **kwargs):
        super(GamesSpider, self).__init__(*args, **kwargs) 
        self.counter = 0
        self.year = year
        self.pace_prev = type(self).get_pace_prev(self.year)
        pickle_name = 'pace_urls_{}.pickle'.format(self.year)
        # urlji za vsako sezono so v pace_urls picklih
        with open(pickle_name, 'rb') as handle:
            self.urls = pickle.load(handle)

        self.start_urls = [self.urls[0]]
        # ['http://www.basketball-reference.com/boxscores/201210310PHO.html']

    @staticmethod
    def get_pace_prev(year):
        """
        Vrne slovar, ki vsebuje povprečen tempo igre iz predhodne sezone za
        vsako ekipo posebej
        
        :param year: niz - predstavlja sezono, za katero računamo povprečen
                     tempo igre za vsako ekipo pred vsako tekmo
        :return: slovar - ključi so kratice imen ekip, vrednosti pa njihovi
                 povprečni tempi iger v predhodni sezoni
        """
        if year == '1516':
            return {'HOU': 96.5, 'DEN': 96.1, 'OKC': 95.7,
                    'GSW': 98.3, 'SAC': 95.4, 'PHX': 96.3,
                    'LAL': 94.0, 'MIN': 94.4, 'LAC': 94.7,
                    'PHI': 95.7, 'BOS': 95.8, 'MIL': 94.1,
                    'DET': 92.8, 'TOR': 92.8, 'DAL': 95.2,
                    'POR': 94.2, 'WAS': 93.7, 'IND': 93.2,
                    'CLE': 92.3, 'CHI': 92.8, 'NOP': 91.4,
                    'ATL': 93.9, 'CHA': 93.0, 'ORL': 93.8,
                    'SAS': 93.8, 'UTA': 90.4, 'BKN': 92.7,
                    'NYK': 91.2, 'MEM': 92.0, 'MIA': 90.9}

        elif year == '1415':
            return {'HOU': 96.3, 'DEN': 98.1, 'OKC': 95.4,
                    'GSW': 96.2, 'SAC': 94.4, 'PHX': 95.8,
                    'LAL': 98.7, 'MIN': 97.3, 'LAC': 95.9,
                    'PHI': 99.2, 'BOS': 93.3, 'MIL': 91.8,
                    'DET': 94.9, 'TOR': 91.8, 'DAL': 93.5,
                    'POR': 94.9, 'WAS': 93.2, 'IND': 92.5,
                    'CLE': 93.1, 'CHI': 90.2, 'NOP': 92.2,
                    'ATL': 94.6, 'CHA': 92.4, 'ORL': 93.6,
                    'SAS': 95.0, 'UTA': 91.4, 'BKN': 91.4,
                    'NYK': 90.3, 'MEM': 89.9, 'MIA': 91.2}

        elif year == '1314':
            return {'HOU': 96.1, 'DEN': 95.1, 'OKC': 93.3,
                    'GSW': 94.5, 'SAC': 93.6, 'PHX': 93.4,
                    'LAL': 94.4, 'MIN': 92.8, 'LAC': 91.1,
                    'PHI': 91.0, 'BOS': 91.7, 'MIL': 94.7,
                    'DET': 90.8, 'TOR': 90.4, 'DAL': 94.1,
                    'POR': 91.4, 'WAS': 92.2, 'IND': 90.2,
                    'CLE': 92.3, 'CHI': 89.3, 'NOP': 88.5,
                    'ATL': 92.6, 'CHA': 91.5, 'ORL': 92.2,
                    'SAS': 94.2, 'UTA': 90.9, 'BKN': 88.8,
                    'NYK': 89.8, 'MEM': 88.4, 'MIA': 90.7}

        elif year == '1213':
            return {'HOU': 91.7, 'DEN': 94.2, 'OKC': 93.0,
                    'GSW': 92.3, 'SAC': 94.7, 'PHX': 92.6,
                    'LAL': 90.5, 'MIN': 93.3, 'LAC': 89.2,
                    'PHI': 89.7, 'BOS': 90.4, 'MIL': 93.7,
                    'DET': 89.2, 'TOR': 89.3, 'DAL': 91.4,
                    'POR': 91.2, 'WAS': 92.5, 'IND': 90.7,
                    'CLE': 91.3, 'CHI': 89.1, 'NOP': 88.3,
                    'ATL': 90.2, 'CHA': 91.1, 'ORL': 89.0,
                    'SAS': 92.9, 'UTA': 91.4, 'BKN': 90.1,
                    'NYK': 93.2, 'MEM': 90.8, 'MIA': 91.2}

    # Za računanje povprečja tempa iz preteklih tekem
    # V vrednostih je prvi element število tekem, drugi pa seštevek vseh
    # vrednosti tempa iger iz posameznih tekem (oboje uporabimo za računanje
    # povprečja)
    pace_sum = {'HOU': [0, 0.0], 'DEN': [0, 0.0], 'OKC': [0, 0.0],
                'GSW': [0, 0.0], 'SAC': [0, 0.0], 'PHX': [0, 0.0],
                'LAL': [0, 0.0], 'MIN': [0, 0.0], 'LAC': [0, 0.0],
                'PHI': [0, 0.0], 'BOS': [0, 0.0], 'MIL': [0, 0.0],
                'DET': [0, 0.0], 'TOR': [0, 0.0], 'DAL': [0, 0.0],
                'POR': [0, 0.0], 'WAS': [0, 0.0], 'IND': [0, 0.0],
                'CLE': [0, 0.0], 'CHI': [0, 0.0], 'NOP': [0, 0.0],
                'ATL': [0, 0.0], 'CHA': [0, 0.0], 'ORL': [0, 0.0],
                'SAS': [0, 0.0], 'UTA': [0, 0.0], 'BKN': [0, 0.0],
                'NYK': [0, 0.0], 'MEM': [0, 0.0], 'MIA': [0, 0.0]}

    # ključi so kratice ekip, vrednosti pa novi slovarji, v katerih so ključi
    # datumi tekem, kjer je ekipa igrala, vrednosti pa povprečni sezonski tempi
    # iger te ekipe do datuma v ključu
    pace_dates = {'HOU': {}, 'DEN': {}, 'OKC': {},
                  'GSW': {}, 'SAC': {}, 'PHX': {},
                  'LAL': {}, 'MIN': {}, 'LAC': {},
                  'PHI': {}, 'BOS': {}, 'MIL': {},
                  'DET': {}, 'TOR': {}, 'DAL': {},
                  'POR': {}, 'WAS': {}, 'IND': {},
                  'CLE': {}, 'CHI': {}, 'NOP': {},
                  'ATL': {}, 'CHA': {}, 'ORL': {},
                  'SAS': {}, 'UTA': {}, 'BKN': {},
                  'NYK': {}, 'MEM': {}, 'MIA': {}}

    @staticmethod
    def fix_teams(pace_list):
        """
        Poenoti imena ekip, tako da ne prihaja do napak
        
        :param pace_list: seznam - ima obliko [(team1, pace), (team2, pace)]
        :return: seznam - vrne enako obliko kot jo ima parameter pace_list, le
                 s poenotenimi imeni
        """
        print(pace_list)
        teams = {'PHO': 'PHX', 'NOH': 'NOP', 'CHO': 'CHA', 'BRK': 'BKN'}
        team1 = pace_list[0][0]
        team2 = pace_list[1][0]
        if team1 in teams.keys():
            team1 = teams[team1]
        if team2 in teams.keys():
            team2 = teams[team2]
        return [(team1, pace_list[0][1]),
                (team2, pace_list[1][1])]

    def update_dicts(self, team, pace, date):
        """
        Posodobi slovarja pace_sum in pace_dates
        
        :param team: niz - kratica ekipe, ki je sodelovala na tekmi
        :param pace: float - tempo igre na tekmi 
        :param date: niz - datum tekme
        :return: None
        """
        # za prvo tekmo v sezoni gre v slovar tempo iz predhodne sezone
        if self.pace_sum[team][0] == 0:
            self.pace_dates[team][date] = self.pace_prev[team]
        # za trenutno tekmo izračunamo povprečen tempo preteklih tekem
        else:
            season_avg = self.pace_sum[team][1] / self.pace_sum[team][0]
            self.pace_dates[team][date] = season_avg
        # posodobimo povprečen tempo z vrednostmi iz trenutne tekme
        self.pace_sum[team][0] += 1
        self.pace_sum[team][1] += pace

    def parse(self, response):
        """
        Iz odgovora izvleče podatke o tempu igre na določeni tekmi ter računa
        in posodablja sezonski tempo igre za vsako ekipo posebej
        :param response: HTTP Response objekt  
        :return: None
        """
        # comment() potegne vse komentarje iz div.id = all_four_factors
        # extract_first() vrne prvi komentar
        xp = '//div[@id="all_four_factors"]/comment()'
        comment = response.xpath(xp).extract_first()

        # pace na tekmi je za obe ekipi vedno enak (ker sta igrali isto tekmo)
        # 1. (.*) = kratica ekipe, 2. (.*) = pace na tekmi
        reg = re.compile(r'<tr >'
                         r'<th scope="row" class="left " data-stat="team_id" >'
                         r'<a href="/teams/.*/.*.html">(.*)</a>'
                         r'</th>'
                         r'<td class="right " data-stat="pace" >(.*)</td>'
                         r'.* data-stat="efg_pct" >')

        # findall v tem primeru vrne 2 ujemanji (<tr> elementov)
        # in jih vrne v seznamu 2-terk
        t = reg.findall(comment)
        [(team1, pace1), (team2, pace2)] = GamesSpider.fix_teams(t)
        date = response.request.url.split('/')[-1][:8]  # naprimer 201210310
        pace = float(pace1)  # pace1 == pace2, ker sta ekipi igrali isto tekmo

        self.update_dicts(team1, pace, date)
        self.update_dicts(team2, pace, date)

        self.counter += 1
        # dokler je število obdelanih urljev manjše od dolžine seznama vseh
        # urljev, se premaknemo na naslednji url
        if self.counter < len(self.urls):
            yield scrapy.Request(self.urls[self.counter], callback=self.parse,
                                 dont_filter=True)

    def close(self, reason):
        """
        Ko pajek preneha delovati, se v pickle zapiše slovar, ki ima v ključih
        kratice ekip, v vrednostih pa so slovarji, kjer so ključi datumi tekem,
        v vrednostih pa povprečen sezonski tempo ekipe do tiste tekme
        
        :param reason: no reason
        :return: None
        """
        pace_dates_name = 'pace_dates_{}.pickle'.format(self.year)
        with open(pace_dates_name, 'wb') as handle:
            pickle.dump(self.pace_dates, handle)